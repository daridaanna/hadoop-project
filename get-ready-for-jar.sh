#!/bin/bash

./hadoop/sbin/stop-all.sh

./clean-up.sh

hdfs namenode -format

./hadoop/sbin/start-dfs.sh

./hadoop/sbin/start-yarn.sh

hadoop fs -mkdir /input

hadoop fs -copyFromLocal input-inverted-index/* /input/

hadoop fs -copyFromLocal stopwords.txt /

