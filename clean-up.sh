#!/bin/bash

rm -rf hadoop/logs/*;
rm -rf hadoop/data/dataNode/*;
rm -rf hadoop/data/namenode/*;

ssh node1 "rm -rf hadoop/logs/*";
ssh node1 "rm -rf hadoop/data/dataNode/*";
