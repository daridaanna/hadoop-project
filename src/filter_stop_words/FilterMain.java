package filter_stop_words;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

@SuppressWarnings("deprecation")
public class FilterMain {
    public static String filteredFolder;

    public enum COUNTERS {
        STOPWORDS,
        GOODWORDS,
        READIN
    }


    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();
        filteredFolder = "filtered_" + args[1];

        Job job = new Job(conf, "filtering");
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        job.setJarByClass(FilterMain.class);
        job.setMapperClass(FilterMapper.class);
        job.setReducerClass(FilterReducer.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);
        job.addCacheFile(new URI("/stopwords.txt"));

        FileInputFormat.setInputPaths(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(filteredFolder));

        job.waitForCompletion(true);
        Counters counters = job.getCounters();
        System.out.printf("Good Words: %d, Stop Words: %d\n",
                counters.findCounter(COUNTERS.GOODWORDS).getValue(),
                counters.findCounter(COUNTERS.STOPWORDS).getValue());

        Configuration configuration = new Configuration();

        Job jobFinal = new Job(configuration, "finalJob");
        jobFinal.setOutputKeyClass(Text.class);
        jobFinal.setOutputValueClass(Text.class);
        jobFinal.setJarByClass(FilterMain.class);

        jobFinal.setMapperClass(WordLineMapper.class);
        jobFinal.setReducerClass(WordLineReducer.class);

        jobFinal.setInputFormatClass(TextInputFormat.class);
        jobFinal.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.setInputPaths(jobFinal, new Path(filteredFolder));
        FileOutputFormat.setOutputPath(jobFinal, new Path(args[1]));

        jobFinal.waitForCompletion(true);

    }
}

