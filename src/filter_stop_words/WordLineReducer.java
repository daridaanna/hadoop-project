package filter_stop_words;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class WordLineReducer extends Reducer<Text, Text, Text, Text> {
    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        Map<String, List<String>> lineFile = new HashMap<>();
        String word = key.toString();

        for (Text value: values) {
            String fileRow = value.toString();

            if (fileRow.contains("#")) {
                String[] fromMapper = fileRow.split("#");
                List<String> list = null;
                if (lineFile.containsKey(fromMapper[0])) {
                    list = lineFile.get(fromMapper[0]);
                } else {
                    list = new LinkedList<>();
                }
                list.add(fromMapper[1]);
                lineFile.put(fromMapper[0], list);
            }
        }

        String finalResult = "[";
        for (Map.Entry<String,List<String>> entry : lineFile.entrySet()) {
            String resultRow = "(";

            String folderName = entry.getKey();
            resultRow += folderName + ",";
            List<String> rowNumberList = entry.getValue();
            for(String lineNr: rowNumberList) {
                resultRow += lineNr + ",";
            }
            resultRow += ")"+ ",";
            finalResult += resultRow;
        }

        finalResult += "]";

        context.write(new Text(word), new Text(finalResult));
    }
}
