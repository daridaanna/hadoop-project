package filter_stop_words;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

public class FilterMapper extends Mapper<LongWritable, Text, Text, Text> {

    private Text word = new Text();
    private Set<String> stopWordList = new HashSet<String>();
    private BufferedReader fis;


    protected void setup(Context context) {
        String nameOfFile = "none";

        try {
            Path[] stopWordFiles = new Path[0];
            stopWordFiles = context.getLocalCacheFiles();

            System.out.println("Read started");
            nameOfFile = stopWordFiles[0].toString();
            fis = new BufferedReader(new FileReader(nameOfFile));
            String stopWord = null;
            while ((stopWord = fis.readLine()) != null) {
                stopWordList.add(stopWord);
                System.out.println(stopWord);
            }
        } catch (IOException ioe) {
            System.err.println("Exception while reading stop word file '"
                    + nameOfFile + "' : " + ioe.toString());
        }
    }

    public void map(LongWritable key, Text value, Context context)
            throws IOException, InterruptedException {
        String line = value.toString();
        StringTokenizer tokenizer = new StringTokenizer(line);
        Long long_number = key.get();
        Text fileName = new Text(((FileSplit) context.getInputSplit()).getPath().getName());

        String result_line = long_number + "#";

        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();
            token = token.replaceAll("[$\\-,;.?!\\'\"\\_:]","");
            token = token.toLowerCase();

            if (stopWordList.contains(token)) {
                context.getCounter(FilterMain.COUNTERS.STOPWORDS)
                        .increment(1L);
            } else {
                context.getCounter(FilterMain.COUNTERS.GOODWORDS)
                        .increment(1L);
                result_line += token + " ";
            }
        }
        context.write(fileName, new Text(result_line));
    }
}
