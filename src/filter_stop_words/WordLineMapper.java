package filter_stop_words;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class WordLineMapper extends Mapper<LongWritable, Text, Text, Text> {
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] splitted = value.toString().split("\t");

        if (splitted.length > 1) {
            String fileName = splitted[0];
            if (splitted[1].contains("#")) {
                String[] nrRow = splitted[1].split("#");
                for (String word: nrRow[1].split(" ")) {
                    context.write(new Text(word), new Text(fileName + "#" + nrRow[0]));
                }
            }
        }
    }
}
