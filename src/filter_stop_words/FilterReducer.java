package filter_stop_words;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class FilterReducer extends Reducer<Text, Text, Text, Text> {
    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        List<Long> keys = new LinkedList<>();
        List<String> rows = new LinkedList<>();

        for(Text val: values) {
            String value = val.toString();
            if (value.contains("#")) {
                keys.add(Long.parseLong(value.split("#")[0]));
            }

            rows.add(value);
        }
        Collections.sort(keys);

        for (String row: rows) {

            if (row.contains("#")) {
                String[] hashString = row.split("#");
                Long keyLong = Long.parseLong(hashString[0]);

                if (keys.contains(keyLong) && hashString.length>1) {
                    context.write(key, new Text(keys.indexOf(keyLong) + "#" + hashString[1]));
                }
            }
        }

    }
}
