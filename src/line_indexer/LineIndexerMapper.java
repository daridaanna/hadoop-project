package line_indexer;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

import java.io.IOException;
import java.util.StringTokenizer;

public class LineIndexerMapper extends MapReduceBase implements Mapper {
    private final static Text word = new Text();
    private final static Text summary = new Text();

    @Override
    public void map(Object object, Object writableObj, OutputCollector output, Reporter reporter)
            throws IOException {
        WritableComparable key = (WritableComparable) object;
        Writable val = (Writable) writableObj;

        String line = val.toString();
        summary.set(key.toString() + ":" + line);
        StringTokenizer itr = new StringTokenizer(line.toLowerCase());
        while(itr.hasMoreTokens()) {
            word.set(itr.nextToken());
            output.collect(word, summary);
        }
    }

}
