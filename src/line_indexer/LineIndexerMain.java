package line_indexer;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class LineIndexerMain {
    public static void main(String[] args) throws IOException {
        JobConf conf = new JobConf(LineIndexerMain.class);
        Job job = new Job(conf);
        conf.setJobName("LineIndexer");
        // The keys are words (strings):
        conf.setOutputKeyClass(Text.class);
        // The values are offsets+line (strings):
        conf.setOutputValueClass(Text.class);
        conf.setMapperClass(LineIndexerMapper.class);
        conf.setReducerClass(LineIndexerReducer.class);
        if (args.length < 2) {
            System.out.println("Usage: LineIndexer <input path> <output path>");
            System.exit(0);
        }
        FileInputFormat.setInputPaths(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        JobClient.runJob(conf);
    }
}
